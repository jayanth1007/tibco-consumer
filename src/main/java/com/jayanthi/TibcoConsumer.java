package com.jayanthi;

import com.tibco.tibjms.naming.TibjmsContext;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

public class TibcoConsumer implements MessageListener {

    public void testConnect() throws NamingException, JMSException {
        Properties env = new Properties( );
        env.put(Context.SECURITY_PRINCIPAL, "halouser");
        env.put(Context.SECURITY_CREDENTIALS, "badpassword1");
        env.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.tibco.tibjms.naming.TibjmsInitialContextFactory");
        env.put(Context.PROVIDER_URL,"ssl://jcia11944:7243");
        env.put(TibjmsContext.SECURITY_PROTOCOL,"ssl");
        env.put(TibjmsContext.SSL_IDENTITY,"/Users/m808421/certificates/tibcocerts/messserv.p12");
        env.put(TibjmsContext.SSL_PASSWORD,"badpassword1");
        env.put(TibjmsContext.SSL_ENABLE_VERIFY_HOST,false);

        InitialContext jndi = new InitialContext(env);

        QueueConnectionFactory queueConnectionFactory = (QueueConnectionFactory) jndi.lookup("SSLQueueConnectionFactory");
        // Create a JMS connection
        QueueConnection connection =
                queueConnectionFactory.createQueueConnection("halouser", "badpassword1");
        connection.start();

        // Create two JMS session objects
        QueueSession queueSession =
                connection.createQueueSession(false,
                        Session.AUTO_ACKNOWLEDGE);

        Queue queue = (Queue) jndi.lookup("TEST_TOPIC");
        QueueReceiver qreceiver = queueSession.createReceiver(queue);
        qreceiver.setMessageListener(this);
        qreceiver.close();
    }

    @Override
    public void onMessage(Message message) {
        System.out.println(message);
    }

    public static void main(String[] args) throws JMSException, NamingException {
        TibcoConsumer consumer = new TibcoConsumer();
        consumer.testConnect();
    }
}
